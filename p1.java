import java.util.*;

class Solution1{
	public static void main(String [] args){
		Scanner sc = new Scanner (System.in);
		System.out.print("Enter number of rows: ");
		int rows = sc.nextInt();
		int x = 1;
		for(int i=1 ; i<=rows;  i++){
			char ch = 'A';
			for(int j = 1;  j<=rows;  j++ ){
				if(i%2==1){
					System.out.print(ch  + " ");
					ch++;
				} else{
					System.out.print( x +" ");
					x=x+2;
				}
			}
			System.out.println();
		}
	}
}
